<?php

namespace App\Services;
use App\Models\Product;

class Cart
{
	/**
	 * Retourne le tableau produits ajouter par l'utilisateur
	 * @return array Data Object
	 */
	public static function get(){
		return [];
	}

	/**
	 * Ajoute un produits dans le paniers
	 */
	public static function add($IDproduct, $quantite){

		if(!is_array($_SESSION['cart'])) {
			$_SESSION['cart'] = [];
		}

		$_SESSION['cart'][$IDproduct] += $quantite; // Sauvegarde le panir dans la session 
	}

	/**
	 * Compte le nombre d'article qu'il y à dans le panier
	 */
	public function count(){
		    $quantite_total=0 ;
		foreach($_SESSION['cart'] as $quantite) {
		
			$quantite_total += $quantite;
			//$Np_produits_total = array_sum($quantite_total);
			
		    
		}
		return $quantite_total;
		
	}

	/**
	 * Calcul le prix total du panier
	 */
	public function total(){
	     	$panier_total =0;
		foreach ($_SESSION['cart'] as $IDproduct => $quantite) {
			$produit = Product::find($IDproduct)->first(); 
			$prix_unitaire = $produit->price;
			$panier_total += $quantite * $prix_unitaire;
		
		}
		return $panier_total;
	}
}