@extends('layouts')

@section('content')
	<section class="container">	
		<h1 class="title">Listes des produits</h1>
		<hr>
		<div class="columns is-desktop">

			@foreach ($products as $product)
			<p>{{$product->name}}</p><br>
			<img src={{$product->picture}}><br></div>
			<h1>{{$product->price}}</h1><br>
			<a href="/product/{{$product->id}}">En savoir plus</a>

			<!-- <form method="Post" action='/order/add'>
			
		<div class="select">
		<select name="quantite">
			<option selected>1</option>
			<option>2</option>
			<option>3</option>
			<option>4</option>
		</select>
		<button class="button">Ajouter au panier</button>
		</div>
		</form> -->
			@endforeach
	
			{{-- Boucles pour récupérer des produits (https://laravel.com/docs/5.8/blade), 
					 Bulma : https://bulma.io/documentation/columns/responsiveness/,
									 https://bulma.io/documentation/components/card/
			 --}}
		</div>
	</section>
@endsection