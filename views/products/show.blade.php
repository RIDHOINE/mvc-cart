@extends('layouts')

@section('content')
	<section class="container">

	<h1 class="title">Information du produit</h1>
	<div><form method="Post" action='/cart/add'>
			<p>{{$product->name}}</p>
			<img src={{$product->picture}}><br></div>
			<h1>{{$product->price}}</h1><br>
			<input type="hidden" name='id' value='{{$product->id}}'>
			<div class="select">
			<select name="quantite">
				<option selected>1</option>
				<option>2</option>
				<option>3</option>
				<option>4</option>
				<option>5</option>
				<option>6</option>
				<option>7</option>
				<option>8</option>
				<option>9</option>
				<option>10</option>
				<option>100</option>

			</select>
			<button type="submit" class="button">Ajouter au panier</button>
			</div>
			</form>
			</div>
		{{-- 
			Affiche toute les information d'un seul produit
			avec un bouton pour ajouter le produits et on peut choisir la quantité
		--}}
	</section>
@endsection